.PHONY: clean

clean:
	rm -fr dist init.done

init:
	terraform init tf; touch init.done

dist:
	mkdir -p dist
	cd go1-dataset && go get . \
		&& GOOS=linux GOARCH=amd64 go build \
			-ldflags="-s -w" \
			-o ../dist/app main.go
	cd dist/ && zip app.zip app && rm -f app

plan:
	terraform plan tf

apply:
	terraform apply -auto-approve tf

destroy:
	terraform destroy -auto-approve tf
